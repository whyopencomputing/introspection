# -*- coding: utf-8 -*-
{
    'name': "introspection",

    'summary': """
        Introspecting Odoo 
        """,

    'description': """
        Take a look in Odoo - the database, the files, ...
    """,

    'author': "Why! Open Computing",
    'website': "http://www.whyopencomputing.ch",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Introspection',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        #'datas/db.xml',
    ],
    'installable': True,
    'application': True,
}