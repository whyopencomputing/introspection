# -*- coding: utf-8 -*-

import os, sys
from lxml import etree
import psycopg2


print "script launched !"
# create the db.xml for the importation
cwd         = os.getcwd(            )
the_path    = "db.xml"
os.remove( the_path                 )
db_xml      = open( the_path , 'w+' )

# create XML tree 
root = etree.Element('odoo')
data        = etree.Element('data'  )
data.text   = " "
root.append(    data                )

#connection to the db
db          = {}
conn        = psycopg2.connect("host=127.0.0.1 dbname=odoo_jean user=odoo_jean password=qwer1234")
cur         = conn.cursor()
cur.execute("SELECT * FROM pg_catalog.pg_tables where schemaname='public' and tablename not like 'introspection%' order by tablename")
table_id    = 0
for record in cur:
    if record[ 0 ] == 'public' :
        #print record
        table_id    +=  1
        db[table_id] = {
            'table_name'    : record[1],
            'has_indexes'   : record[4],
            'has_rules'     : record[5],
            'has_trigger'   : record[6],
            'cols'          : None,
            'relation'      : None,            
            }    
#print table_id
turn                = 1
for table_index in db:    
        cols        =   []
        record      =   etree.Element('record' , model="introspection.introspectiondb", id="introspection.introspectiondb_" + str( turn )  )
        table_name  =   etree.Element('field', name="table_name")
        table_name.text = "%s"%(  db[table_index]['table_name']   )   
        record.append( table_name )
        cur.execute(    "SELECT column_name FROM information_schema.columns WHERE table_name ='%s';"%(  db[table_index]['table_name']   )   )
        for col in cur:
            cols.append( col[0] )
        #print cols
        db[table_index][ 'cols' ] = cols
        table_cols  =   etree.Element('field', name="table_cols")
        table_cols.text = "%s"%(  cols   )   
        record.append( table_cols )
        rels        =   []
        req         =   ""
        req         +=  "SELECT tc.constraint_name, tc.table_name, kcu.column_name, "
        req         +=  "ccu.table_name AS foreign_table_name, ccu.column_name AS foreign_column_name "
        req         +=  "from "
        req         +=  "information_schema.table_constraints AS tc JOIN information_schema.key_column_usage AS kcu "
        req         +=  "ON tc.constraint_name = kcu.constraint_name "
        req         +=  "JOIN information_schema.constraint_column_usage AS ccu "
        req         +=  "ON ccu.constraint_name = tc.constraint_name "
        req         +=  "WHERE constraint_type = 'FOREIGN KEY' AND tc.table_name='%s' "%(  db[table_index]['table_name']   )
        req         +=  "and tc.constraint_name not like '%create_uid_fkey' "
        req         +=  "and tc.constraint_name not like '%write_uid_fkey' ;"
        cur.execute(    req     )    
        for rel in cur:
            this_rel = {
                'col_name'  :   rel[2],
                'f_table'   :   rel[3],
                'f_col'     :   rel[4],
                } 
            rels.append( this_rel )      
        db[table_index][ 'relation' ] = rels
        table_relation  =   etree.Element('field', name="table_relation")
        table_relation.text = "%s"%(  rels   )   
        record.append( table_relation ) 
        table_has_index  =   etree.Element('field', name="table_has_index")
        table_has_index.text = "%s"%(  db[table_index]['has_indexes']   )   
        record.append( table_has_index )
        table_has_rules  =   etree.Element('field', name="table_has_rules")
        table_has_rules.text = "%s"%(  db[table_index]['has_rules']   )   
        record.append( table_has_rules )
        table_has_trigger  =   etree.Element('field', name="table_has_trigger")
        table_has_trigger.text = "%s"%(  db[table_index]['has_trigger']   )   
        record.append( table_has_trigger )
        print "turn " + str(turn)
        turn        +=  1
        data.append( record )
print "done !"
#print db

"""         
SELECT
    tc.constraint_name, tc.table_name, kcu.column_name, 
    ccu.table_name AS foreign_table_name,
    ccu.column_name AS foreign_column_name 
FROM 
    information_schema.table_constraints AS tc 
    JOIN information_schema.key_column_usage AS kcu
      ON tc.constraint_name = kcu.constraint_name
    JOIN information_schema.constraint_column_usage AS ccu
      ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY' AND tc.table_name='mytable';
"""      

# writing the file
s           = etree.tostring(root, pretty_print=True)
db_xml.write( s )
db_xml.close(   )
    