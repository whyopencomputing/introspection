# -*- coding: utf-8 -*-

from odoo import models, fields, api

import logging
_logger     = logging.getLogger(__name__)

class introspectionDB(models.Model):
     _name = 'introspection.introspectiondb'

     table_name         = fields.Text()
     table_cols         = fields.Text()
     table_relation     = fields.Text()
     table_files        = fields.Text()
     table_has_index    = fields.Boolean()
     table_has_rules    = fields.Boolean()
     table_has_trigger  = fields.Boolean()
     
     